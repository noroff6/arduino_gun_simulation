# Arduino Gun Simulation
Created using [Wokwi](https://wokwi.com/). Try out the simulation [here](https://wokwi.com/projects/344695374375551572).


## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy for Embedded Development](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Image](#image)
- [Assignment](#assignment)
- [Implementation](#implementation)
- [Resources](#resources)
- [Contributors](#contributors)


## Image
![](image.PNG)

### Assignment
Create a simulation of the gun:
- two serwo motors;
- 4 buttons controlling motors;
- a fire button;
- if the fire button is pressed: active buzzer; LED and show information on the LCD display;
- LCD display is showing how much ammuntion is left;

Bonus:
- Control gun using joystick;


## Implementation
Implemented using Wokwi and C/C++. Can be transferred to an Arduino project, or simply run on Wokwi. 
The project has 2 different servos connected to left/right push buttons. One servo is also connected to a joystick. The "gun" can be driven to the different angles using the buttons or/and joystick. A button for firing is present and a LCD screen which states how much ammunition there is left. A buzzer will sound each time the gun fires.

## Resources
- [Custom smiley generator](https://maxpromer.github.io/LCD-Character-Creator/)
- [Wokwi](https://wokwi.com/)


## Contributors
This is a project created by [Helena Barmer](https://gitlab.com/helenabarmer) and [Joel Fredin](https://gitlab.com/joelfredin).
For any questions reach out to us.



