#include <LiquidCrystal.h>
#include <Servo.h>
#include "pitches.h"


// Push buttons
int fireAmmoBtn = A4;
int leftTopBtn = A3;
int leftBottomBtn = A2;
int rightTopBtn = A1;
int rightBottomBtn = A0;

int robotArmTop = 12;
int robotArmBottom = 13;

Servo arm; // Create a "Servo" object called "arm"
float pos = 90.0; // Variable where the arm's position will be stored (in degrees)
float step = 1.0; // Variable used for the arm's position step

Servo arm2; // Create a "Servo" object called "arm"
float pos2 = 90.0; // Variable where the arm's position will be stored (in degrees)
float step2 = 1.0; // Variable used for the arm's position step

bool enemy = false;
byte ammo = 3;

// States
int state;
const int fireBtnState = 0;
const int startUpState = 1;
const int leftTopServoState = 3;
const int rightTopServoState = 4;
const int leftBottomServoState = 5;
const int rightBottomServoState = 6;

#define VERT_PIN A0
#define HORZ_PIN A2
#define SEL_PIN  10

// LCD
LiquidCrystal lcd(8, 7, 6, 5, 4, 3);

int buzzerPin = 9;

int totalAmmo = 3;
String lcdText = "Total ammo left " + String(totalAmmo);

// Time
unsigned long previousMillis = 0;        
const long interval = 3000; 

byte smiley[] = {
  B00000,
  B00000,
  B01010,
  B00000,
  B10001,
  B01110,
  B00000,
  B00000
};



void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(fireAmmoBtn, INPUT_PULLUP);
  pinMode(leftTopBtn, INPUT_PULLUP);
  pinMode(leftBottomBtn, INPUT_PULLUP);
  pinMode(rightTopBtn, INPUT_PULLUP);
  pinMode(rightBottomBtn, INPUT_PULLUP);
  pinMode(buzzerPin, OUTPUT);

  //pinMode(VERT_PIN, INPUT);
  //pinMode(HORZ_PIN, INPUT);
  pinMode(SEL_PIN, INPUT_PULLUP);

  arm.attach(robotArmTop); // Attache the arm to the pin 2
  arm.write(pos); // Initialize the arm's position to 0 (leftmost)

  arm2.attach(robotArmBottom);
  arm2.write(pos2);

  state = startUpState;

}

void loop() {
  unsigned long currentMillis = millis();

  if(digitalRead(fireAmmoBtn) == 0)
  {
    state = fireBtnState;
  }
  if(!digitalRead(leftTopBtn)){
    state = leftTopServoState;
  }
  if (!digitalRead(rightTopBtn)){
    state = rightTopServoState;
  }
  if (!digitalRead(leftBottomBtn)){
    state = leftBottomServoState;
  }
  if (!digitalRead(rightBottomBtn)){
    state = rightBottomServoState;
  }

  switch(state){
    case fireBtnState:
    fireAmmo();
    if (currentMillis - previousMillis >= interval){
        previousMillis = currentMillis;
        totalAmmo--;
        if(totalAmmo <1)
        {
          lcd.createChar(9, smiley);
          lcdText = "Re-fill \x09 ammo!";
        }
        else{
          lcdText = "Total ammo left " + String(totalAmmo);
        }
        
        state = startUpState;
        
      }
      
      
    break;

    case startUpState:
    startSession();
    break;

    case leftTopServoState:
    servoTopLeft();
    state = startUpState;
    break;

    case rightTopServoState:
    servoTopRight();
    state = startUpState;
    break;

    case leftBottomServoState:
    servoBottomLeft();
    state = startUpState;
    break;

    case rightBottomServoState:
    servoBottomRight();
    state = startUpState;
    break;

  }

  int vert = analogRead(HORZ_PIN);
  int horz = analogRead(VERT_PIN);

  if(vert < 300)
  {
    if(pos2 < 0)
    {       
    }
    else
    {
      pos2-=step2;
      arm2.write(pos2);
      delay(5); 
    }
  }
  if(vert > 700)
  {
    if (pos2 > 180)
    {

    }
    else
    {
      pos2+=step2;
      arm2.write(pos2);
      delay(5);
    }
  }

  if(horz < 300)
  {
    if(pos2 < 0)
    {       
    }
    else
    {
      pos2-=step2;
      arm2.write(pos2);
      delay(5); 
    }
  }
  if(horz > 700)
  {
    if (pos2 > 180)
    {

    }
    else
    {
      pos2+=step2;
      arm2.write(pos2);
      delay(5);
    }
  }
}

void servoTopLeft()
{
  if (pos>-90) // Check that the position won't go lower than 0°
    {
      arm.write(pos); // Set the arm's position to "pos" value
      pos-=step; // Decrement "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
}

void servoTopRight()
{
  if (pos<180) // Check that the position won't go higher than 180°
    {
      arm.write(pos); // Set the arm's position to "pos" value
      pos+=step; // Increment "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
}

void servoBottomLeft(){
  if (pos2>-90) // Check that the position won't go lower than 0°
    {
      arm2.write(pos2); // Set the arm's position to "pos" value
      pos2-=step2; // Decrement "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
}

void servoBottomRight(){
   if (pos2<180) // Check that the position won't go higher than 180°
    {
      arm2.write(pos2); // Set the arm's position to "pos" value
      pos2+=step2; // Increment "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
}

void fireAmmo(){
  
  if(totalAmmo < 1){
    enemy = false;
    updateLCD("No more ammo!", enemy);
  }
  else{
    tone(buzzerPin, NOTE_B3);
    enemy = false;
    updateLCD("FIRE!", enemy);
    //tone(buzzerPin, NOTE_C4);
    }
}

void startSession(){
  enemy = true;
  updateLCD(lcdText, enemy);
  noTone(buzzerPin);
}


void updateLCD(String text, bool enemy){
  static unsigned long lcdTimer = 0;
   unsigned long lcdInterval = 500; 
   if (millis() - lcdTimer >= lcdInterval)
   {
     if(enemy)
      {
        lcdTimer = millis();
        lcd.setCursor(1, 16);
        lcd.print(text);
        lcd.scrollDisplayLeft();
      }
      else{
      lcd.clear();
      lcdTimer = millis();
      lcd.setCursor(1, 16);
      lcd.print(text);
      }
   }
}
